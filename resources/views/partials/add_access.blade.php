<!-- Modal -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Добавление доступов</h4>


    <div class="modal-content">
        <ul class="nav nav-tabs navtab-bg nav-justified">
            <li class="active">
                <a href="#home-2" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Хостинг</span>
                </a>
            </li>
            <li class="">
                <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Ftp</span>
                </a>
            </li>
            <li class="">
                <a href="#messages-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                    <span class="hidden-xs">Сайт/админка</span>
                </a>
            </li>
            <li class="">
                <a href="#settings-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-cog"></i></span>
                    <span class="hidden-xs">Другое</span>
                </a>
            </li>
        </ul>
        <div class="tab-content" style="padding-top: 15px;">
            <div class="tab-pane active" id="home-2">
                <h3 class="text-left">Доступы на Хостинг</h3>

                <form id="add_host">
                    <div class="form-group">
                        <input type="text" name="host"  class="form-control" placeholder="Hosting url" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="login"  class="form-control"  placeholder="Login" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="password"  class="form-control"  placeholder="Password" required>
                    </div>

                    <button class="btn btn-primary waves-effect waves-light">
                        Сохранить
                        <i class="md md-save"></i>
                    </button>

                </form>
            </div>
            <div class="tab-pane" id="profile-2">
                <h3 class="text-left">Доступы на Ftp</h3>
                <form id="add_ftp">
                    <div class="form-group">
                        <input type="text" name="name"  class="form-control" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="host"  class="form-control" placeholder="Ftp host" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="login"  class="form-control"  placeholder="Login" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="password"  class="form-control"  placeholder="Password" required>
                    </div>

                    <button class="btn btn-primary waves-effect waves-light">
                        Сохранить
                        <i class="md md-save"></i>
                    </button>
                </form>
            </div>
            <div class="tab-pane " id="messages-2">
                <h3 class="text-left">Доступы в админку сайта</h3>
                <form id="add_site">

                    <div class="form-group">
                        <input type="text" name="host"  class="form-control" placeholder="Admin panel url" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="login"  class="form-control"  placeholder="Login" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="password"  class="form-control"  placeholder="Password" required>
                    </div>

                    <button class="btn btn-primary waves-effect waves-light">
                        Сохранить
                        <i class="md md-save"></i>
                    </button>

                </form>

            </div>
            <style>
                textarea {
                    outline: none;
                    resize: none;
                }
            </style>
            <div class="tab-pane" id="settings-2">
                <h3 class="text-left">Другие доступы</h3>
                <form id="add_other">
                    <div class="form-group">
                        <input type="text" name="name"  class="form-control" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <p class="text-left">  <label for="other_description" > Описание доступов</label></p>

                        <textarea name="other_description" class="form-control" id="other_description" cols="30" rows="5">

                        </textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" name="host"  class="form-control" placeholder="Access url" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="login"  class="form-control"  placeholder="Login" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="password"  class="form-control"  placeholder="Password" required>
                    </div>

                    <button class="btn btn-primary waves-effect waves-light">
                        Сохранить
                        <i class="md md-save"></i>
                    </button>
                </form>

            </div>
        </div>
    </div>
</div>

<a href="#custom-modal" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal" data-overlayspeed="200" data-overlaycolor="#36404a">Добавить</a>
