<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            @if (Auth::check())
                <ul>
                    <li class="text-muted menu-title">Navigation</li>
            @role('admin')


                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i> <span> Users </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('admin.users')}}">All users</a></li>
                        </ul>
                    </li>


            @endrole

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="ti-layers-alt"></i> <span> Projects </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('projects')}}">All projects</a></li>
                        </ul>
                    </li>

                </ul>
            @endif
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->