</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/detect.js') }}"></script>
<script src="{{ asset('assets/js/fastclick.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('assets/js/waves.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>




<script src="{{ asset('assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('assets/js/jquery.app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>

{{--<script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>--}}

<script src="{{ asset('assets/plugins/custombox/js/custombox.min.js') }}"></script>
<script src="{{ asset('assets/plugins/custombox/js/legacy.min.js') }}"></script>




<script src="{{ asset('assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
{{--<script type="text/javascript" src="{{ asset('assets/plugins/autocomplete/jquery.mockjax.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/autocomplete/countries.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/pages/autocomplete.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('assets/pages/jquery.form-advanced.init.js') }}"></script>

<script type="text/javascript">
    !function ($) {
        "use strict";
        var SweetAlert = function () {
        };

        //examples
        SweetAlert.prototype.init = function () {

            //Success Message
            $('#sa-success').click(function () {

            });
        },
            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),
//initializing
        function ($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
        $(document).ready(function () {

        $('[data-clipboard-target]').click(function( event ) {
            event.preventDefault();
        });
        var clipboardDemos = new Clipboard('[data-clipboard-target]');
        clipboardDemos.on('success', function(e) {
            swal("Скопировано", "", "success");

            setTimeout(function () {
                swal.close();
            },1000);

            e.clearSelection();
            console.info('Action:', e.action);
            console.info('Text:', e.text);
            console.info('Trigger:', e.trigger);
        });
        clipboardDemos.on('error', function(e) {
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
            //showTooltip(e.trigger, fallbackMessage(e.action));
        });

    });
</script>

<script>
    $(document).ready(function () {
        $('.admin_site').submit(function () {
            $host = $(this).find( "input[name^='site_admin_']").val();
            $login = $(this).find( "input[name^='site_login_']").val();
            $password = $(this).find( "input[name^='site_password_']").val();
            $access_id = $(this).find( "input[name^='access_id']").val();
            $.ajax({
                url: '{{route('update.project')}}',
                method: 'POST',
                data: {
                    'access_type' : 'site',
                    'access_id' : $access_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                    },1000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });

        $('.hosting_form').submit(function () {
            $host = $(this).find( "input[name^='hosting_host_']").val();
            $login = $(this).find( "input[name^='hosting_login_']").val();
            $password = $(this).find( "input[name^='hosting_password_']").val();
            $access_id = $(this).find( "input[name^='access_id']").val();

            $.ajax({
                url: '{{route('update.project')}}',
                method: 'POST',
                data: {
                    'access_type' : 'hosting',
                    'access_id' : $access_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                    },1000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });
        $('.ftp_form').submit(function () {

        $host = $(this).find( "input[name^='ftp_host_']").val();
        $login = $(this).find( "input[name^='ftp_login_']").val();
        $password = $(this).find( "input[name^='ftp_password_']").val();
        $access_id = $(this).find( "input[name^='access_id']").val();

           $.ajax({
                url: '{{route('update.project')}}',
                method: 'POST',
                data: {
                    'access_type' : 'ftp',
                    'access_id' : $access_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                    },1000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });
        $('.other_access').submit(function () {

            $host = $(this).find( "input[name^='other_access_url_']").val();
            $login = $(this).find( "input[name^='other_access_login_']").val();
            $password = $(this).find( "input[name^='other_access_password_']").val();
            $access_id = $(this).find( "input[name^='access_id_']").val();

            $.ajax({
                url: '{{route('update.project')}}',
                method: 'POST',
                data: {
                    'access_type' : 'other',
                    'access_id' : $access_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                    },1000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });


        /*Добавление доступов*/

        $('#add_host').submit(function () {
            $host = $(this).find( "input[name='host']").val();
            $login = $(this).find( "input[name='login']").val();
            $password = $(this).find( "input[name='password']").val();
            $project_id = $("input[name='project_id']").val();
            $.ajax({
                url: '{{route('add.access')}}',
                method: 'POST',
                data: {
                    'access_type' : 'hosting',
                    'project_id' : $project_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    Custombox.close();
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                        location.reload();
                    },2000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });
        $('#add_site').submit(function () {
            $host = $(this).find( "input[name='host']").val();
            $login = $(this).find( "input[name='login']").val();
            $password = $(this).find( "input[name='password']").val();
            $project_id = $("input[name='project_id']").val();
            $.ajax({
                url: '{{route('add.access')}}',
                method: 'POST',
                data: {
                    'access_type' : 'site',
                    'project_id' : $project_id,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    Custombox.close();
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                        location.reload();
                    },2000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });
        $('#add_ftp').submit(function () {
            $name = $(this).find( "input[name='name']").val();
            $host = $(this).find( "input[name='host']").val();
            $login = $(this).find( "input[name='login']").val();
            $password = $(this).find( "input[name='password']").val();
            $project_id = $("input[name='project_id']").val();

            $.ajax({
                url: '{{route('add.access')}}',
                method: 'POST',
                data: {
                    'access_type' : 'ftp',
                    'project_id' : $project_id,
                    'name' : $name,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    Custombox.close();
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                        location.reload();
                    },2000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });
        $('#add_other').submit(function () {
            $name = $(this).find( "input[name='name']").val();
            $other_description = $(this).find( "textarea[name='other_description']").val();
            $host = $(this).find( "input[name='host']").val();
            $login = $(this).find( "input[name='login']").val();
            $password = $(this).find( "input[name='password']").val();
            $project_id = $("input[name='project_id']").val();
            $.ajax({
                url: '{{route('add.access')}}',
                method: 'POST',
                data: {
                    'access_type' : 'other',
                    'project_id' : $project_id,
                    'name' : $name,
                    'description' : $other_description,
                    'host' : $host,
                    'login' : $login,
                    'password' : $password,
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    Custombox.close();
                    console.log(data);
                    swal("Сохранено", "", "success");

                    setTimeout(function () {
                        swal.close();
                        location.reload();
                    },2000);

                },
                error: function(msg){
                    console.log(msg);
                }
            })
            return false;
        });

    }(window.jQuery));

</script>

@yield('scripts')

</body>
</html>