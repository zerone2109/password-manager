
<!-- Modal -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Добавление проекта</h4>
    <div class="custom-modal-text">
        <form id="add-pr-form">

            <input type="text" name="name" placeholder="Имя" class="form-control" required>
            <br>
            <p class="text-left" style="margin-bottom: 0;"><label for="">Описание проекта</label></p>
            <textarea name="description" class="form-control" rows="5"></textarea>

            <p class="text-left" style="margin-top: 15px;"> <button type="submit" class="btn btn-primary">Сохранить</button></p>

        </form>


    </div>
</div>


<a href="#custom-modal" class="btn btn-primary waves-effect waves-light" data-animation="fadein" data-plugin="custommodal"
   data-overlaySpeed="200" data-overlayColor="#36404a">Add project</a>
