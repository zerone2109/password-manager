@if($users != 'Nothing found')
<div class="table-responsive">
    <table class="table table-hover mails m-0 table table-actions-bar">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Start Date</th>
            <th style="min-width: 90px;">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>
                {{$user->name}}
            </td>
            <td>
                <a href="{{route('admin.user',['id' => $user->id])}}"> {{$user->email}}</a>
            </td>
            <td>
                {{$user->created_at}}
            </td>
            <td>

                <a href="{{route('admin.user',['id' => $user->id])}}" class="table-action-btn"><i class="md md-edit"></i></a>
                <a href="{{ route('delete.user',['id' => $user->id]) }}" class="table-action-btn delete-user"><i class="md md-close"></i></a>

                <form id="delete-user-{{$user->id}}" action="{{ route('delete.user',['id' => $user->id]) }}" method="POST" style="display: none;">
                    <input type="hidden" name="_Token" value="{{ csrf_token() }}">
                </form>
            </td>
        </tr>
       @endforeach
        </tbody>
    </table>

</div>
<hr>
{{ $users->links() }}
    @else
    {{ $users }}
@endif
