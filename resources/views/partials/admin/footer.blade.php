<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script><!-- Popper for Bootstrap -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!-- jQuery  -->
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>

<script src="{{asset('assets/plugins/autocomplete/jquery.autocomplete.min.js')}}"></script>

<script src="{{asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('assets/plugins/raphael/raphael-min.js')}}"></script>

<script src="{{asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>

<!-- Todojs  -->
<script src="{{asset('assets/pages/jquery.todo.js')}}"></script>
<script src="{{asset('assets/plugins/switchery/js/switchery.min.js')}}"></script>
<!-- chatjs  -->
<script src="{{asset('assets/pages/jquery.chat.js')}}"></script>

<script src="{{asset('assets/plugins/peity/jquery.peity.min.js')}}"></script>

<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>


<!-- Modal-Effect -->
<script src="{{asset('assets/plugins/custombox/js/custombox.min.js')}}"></script>
<script src="{{asset('assets/plugins/custombox/js/legacy.min.js')}}"></script>

<!-- Sweet-Alert  -->
<script src="{{asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
{{--CKeditor--}}
<script src="{{asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{asset('assets/plugins/phone-mask/js/intlTelInput.min.js')}}"></script>

<script>


    $('.summernote_area').each(function (e) {
        var id = this.id;
        $('#'+id).summernote({
            height:300
        });
    });

</script>
<script>
    $(function () {
        var telInput = $("#phone"),
            errorMsg = $("#error-msg"),
            validMsg = $("#valid-msg");
        // initialise plugin
        telInput.intlTelInput({
            initialCountry: "auto",
            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "{{asset('assets/plugins/phone-mask/js/utils.js')}}",
            nationalMode: false,
            formatOnDisplay: false
        });
        var reset = function() {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
            validMsg.addClass("hide");

        };

        // on blur: validate
        telInput.blur(function() {
            reset();
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                   /* var interPhone = $("#phone").intlTelInput("getNumber");
                   $("#phone").val(interPhone);*/
                    validMsg.removeClass("hide");
                    $('button[type="submit"]').removeAttr('disabled');
                } else {
                    $('button[type="submit"]').attr('disabled','disabled');
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                }
            }
        });
        reset();
// on keyup / change flag: reset
        telInput.on("keyup change", reset);




    })
</script>

@yield('scripts')

</body>
</html>