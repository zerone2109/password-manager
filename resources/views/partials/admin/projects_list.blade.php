@if($projects != 'Nothing found')
    <div class="row">
        @foreach($projects as $project)

                <div class="col-lg-4">
                    <div class="portlet">
                        <div class="portlet-heading bg-inverse">
                            <h3 class="portlet-title">
                                <a href="{{ route('show', ['$project_id' => $project->id]) }}"> {{$project->name}}</a>
                            </h3>
                            @permission('create-project')
                            <div class="portlet-widgets">
                                <a href="#" class="remove-project" data-project="{{$project->id}}"><i class="ion-close-round"></i></a>
                            </div>
                            @endpermission
                            <div class="clearfix"></div>
                        </div>
                        <div id="bg-primary" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <a href="{{ route('show', ['$project_id' => $project->id]) }}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>

        @endforeach
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{ $projects->links() }}
        </div>

    </div>
@endif