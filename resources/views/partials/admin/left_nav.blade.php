<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                @role(['admin','editor'])
                <li>
                    <a href="{{route('admin.dashboard')}}" class="waves-effect"><i class="fa fa-home"></i> <span> Dashboard </span></a>
                </li>
                @endrole
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user"></i><span> Profile </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li> <a href="{{ route('show.profile') }}">Edit Profile</a></li>


                    </ul>
                </li>
                @role(['admin'])
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Users </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('admin.admins')}}">Admins</a></li>
                        <li><a href="{{route('admin.editors')}}">Editors</a></li>
                        <li><a href="{{route('admin.users')}}">Registered Users</a></li>
                    </ul>
                </li>
                @endrole
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->