
@forelse($posts as $post)
    <div class="blog-box-one col-md-6">

        <div class="post-info">
            <div class="date">
                <span class="day">{{$post->created_at->format('d')}}</span><br>
                <span class="month">{{$post->created_at->format('M')}}</span>
            </div>

            <div class="meta-container">
                <a href="/{{$post->url}}" target="_blank">
                    <h4 class="text-overflow text-dark font-18 font-600 m-t-0">{{$post->title}}  <i class="mdi md-open-in-new"></i></h4>
                </a>
                <div class="font-13">

                    <span class="meta">Last updated by <b>{{$post->user->name}}</b></span>
                </div>
            </div>

            <p class="text-muted m-b-0">

                {!! substr(html_entity_decode(strip_tags($post->content)),0,450).'...' !!}
            </p>

            <div class="row m-t-10">
                <div class="col-6">
                    {{--<div class="m-t-10 blog-widget-action">--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--<i class="mdi md-favorite"></i> <span>54</span>--}}
                        {{--</a>--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--<i class="mdi md-comment"></i> <span>26</span>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                </div>
                <div class="col-6 text-right">
                    <a href="{{route('admin.edit-post',$post->id)}}" class="btn btn-sm waves-effect btn-white">Edit</a>
                </div>
            </div>
        </div>

    </div>

    @empty
    <h4>Post not founds...</h4>
@endforelse
{{--@if($posts)--}}
<div class="col-md-12 justify-content-center">
    {{ $posts->appends(request()->input())->links() }}
</div>
{{--@endif--}}