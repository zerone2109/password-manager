@include('partials.admin.header')
<!-- Begin page -->
<div id="wrapper">
    @auth
    {{--@role(['admin','editor'])--}}
    @include('partials.admin.topbar')
    @include('partials.admin.left_nav')
    @endauth
    {{--@endrole--}}
@yield('content')
{{--    @include('partials.admin.right_nav')--}}
</div>
<!-- END wrapper -->
@include('partials.admin.footer')