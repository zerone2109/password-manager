@extends('layouts.app')

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12">

                        <h4 class="page-title">{{ $title }} &nbsp; @role('admin')<span class="label label-default">Admin</span>@endrole @role('editor')<span class="label label-default">Editor</span>@endrole</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form role="form"  id="edit-profile" method="POST">
                                        <input type="hidden" name="_Token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" value="{{ $user->name }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ $user->email }}" required>
                                        </div>
                                       {{-- <div class="form-group">
                                            <label for="phone">Phone number</label><br>
                                            <input type="tel" id="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $user->phone }}" required>
                                            <span id="valid-msg" class="hide">✓ Valid</span>
                                            <span id="error-msg" class="hide">Invalid number</span>
                                        </div>--}}
                                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                                    </form>
                                    <hr>
                                    <a href="{{route('change.password')}}" class="btn btn-default waves-effect waves-light"> <i class="fa fa-edit m-r-5"></i> <span>Change password</span> </a>
                                </div>
                                <div class="col-sm-4">

                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; 2016 - 2018. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection


@section('scripts')

    <script>
        $(function () {
            $('#edit-profile').submit(function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{route('edit.profile')}}",
                    method: 'POST',
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        console.log(data);
                        if (data != 1) {
                            var message;
                            if (typeof data === 'object') {
                                for (key in data) {
                                    message += data[key]
                                }
                            }
                            else {
                                message = data;
                            }

                            swal({
                                title: 'Error!',
                                text: message,
                                type: 'error',
                                timer: 3000,
                                onClose: function () {

                                }
                            }).catch(swal.noop);
                        }
                        else {
                            swal({
                                title: 'Success!',
                                text: 'Your profile is updated.If you changed the phone you need to re-subscribe to the bot telegram',
                                type: 'success',
                                timer: 8000,
                                onClose: function () {

                                }
                            }).catch(swal.noop);
                        }
                    },
                    error: function (msg) {
                        console.log(msg);
                    }
                })
            });
        })
    </script>

@endsection