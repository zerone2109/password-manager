@extends('layouts.app')

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12">

                        <h4 class="page-title">{{$title or 'Users'}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form role="form" id="search-user">
                                        <div class="form-group contact-search m-b-30">
                                            <input type="hidden" name="role" value="{{$role or 0}}">
                                            <input type="text" id="search" name="query" class="form-control" placeholder="Search...">
                                            <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                        </div> <!-- form-group -->
                                    </form>
                                </div>
                                <div class="col-sm-4">
                                    <a href="#custom-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal"
                                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add User</a>
                                </div>
                            </div>
                            <div id="user-results">
                                @include('partials.admin.users_list')
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <!-- Modal -->
                <div id="custom-modal" class="modal-demo">
                    <button type="button" class="close" onclick="Custombox.close();">
                        <span>&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="custom-modal-title">Add User</h4>
                    <div class="custom-modal-text text-left">
                        <form role="form" id="add-user" method="POST">
                            <input type="hidden" name="_Token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password *</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required>
                            </div>
                            <div class="form-group">
                                <label for="role">User Role</label>
                                <select name="role" id="role" class="form-control" required>
                                    <option value="#" disabled selected>Select role</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light m-l-10" onclick="Custombox.close();">Cancel</button>
                        </form>
                    </div>
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; 2016 - 2018. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection



@section('scripts')

    <script>
        $(function () {

            $(document).on('click', '.delete-user', function () {
                event.preventDefault();
                confirmdelete($(this).attr('href'));
            });

            function confirmdelete($route) {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes!'
                }).then(function () {
                    $.ajax({
                        url: $route,
                        method: 'POST',
                        data: {},
                        headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    });
                    swal({
                    title: 'Deleted!',
                    text: 'User has been deleted.',
                    type: 'success',
                    timer: 3000,
                    onClose: function () {
                        location.reload();
                    }
                    });
                }).catch(swal.noop);
            }

            $('#add-user').submit(function (event) {
            event.preventDefault();
            $.ajax({
            url: "{{route('add.user')}}",
            method: 'POST',
            data: $(this).serialize(),
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
            console.log(data);

            if (data != 1) {
            var message;
            if (typeof data === 'object') {
            for (key in data) {
            message += data[key] + ', '
            }
            }
            else {
            message = data;
            }

            Custombox.close();
            swal({
            title: 'Error!',
            text: message,
            type: 'error',
            timer: 3000,
            onClose: function () {

            }
            }).catch(swal.noop);
            }
            else {
            Custombox.close();

            swal({
            title: 'Success!',
            text: 'User successfully added.',
            type: 'success',
            timer: 3000,
            onClose: function () {
            location.reload();
            }
            }).catch(swal.noop);
            }

            },
            error: function (msg) {
            console.log(msg);
            }
            })
            });


            $('#search-user').submit(function (event) {
            event.preventDefault();
            $.ajax({
            url: "{{route('search.user')}}",
            method: 'POST',
            data: $(this).serialize(),
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
            $('#user-results').html('');
            $('#user-results').html(data);
            console.log(data);
            },
            error: function (msg) {
            console.log(msg);
            }
            })
            });

        })
    </script>

@endsection