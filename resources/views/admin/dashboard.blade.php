@extends('layouts.admin_layout')

@section('content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12">
                        <h4 class="page-title">Dashboard</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="widget-panel widget-style-2 bg-white">
                            <a href="{{route('admin.posts')}}"><i class="md md-find-in-page text-primary"></i></a>
                            <h2 class="m-0 text-dark counter font-600">{{ $posts->count() }}</h2>
                            <div class="text-muted m-t-5">Total Posts</div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-3">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="md md-account-child text-custom"></i>
                            <h2 class="m-0 text-dark counter font-600">{{ $users->count() }}</h2>
                            <div class="text-muted m-t-5">Users</div>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; 2016 - 2018. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection