@extends('layouts.app')

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12">

                        <h4 class="page-title">Edit user {{ $user->name }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form role="form" id="edit-user" method="POST">
                                        <input type="hidden" name="_Token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" value="{{ $user->name }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ $user->email }}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="role">User Role</label>
                                            <select name="role" id="role" class="form-control" required>
                                                <option value="#" disabled>Select role</option>
                                                @foreach($roles as $role)
                                                    <option @if($user->hasRole($role->name)) selected @endif value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                                    </form>
                                </div>
                                <div class="col-sm-4">

                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; 2016 - 2018. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection


@section('scripts')

    <script>
        $(function (){
            $('#edit-user').submit(function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{route('edit.user')}}",
                    method: 'POST',
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        console.log(data);
                        if (data != 1) {
                            var message;
                            if (typeof data === 'object') {
                                for (key in data) {
                                    message += data[key]
                                }
                            }
                            else {
                                message = data;
                            }

                            swal({
                                title: 'Error!',
                                text: message,
                                type: 'error',
                                timer: 3000,
                                onClose: function () {

                                }
                            }).catch(swal.noop);
                        }
                        else {


                            swal({
                                title: 'Success!',
                                text: 'User successfully updated.',
                                type: 'success',
                                timer: 3000,
                                onClose: function () {
                                    location.reload();
                                }
                            }).catch(swal.noop);
                        }
                    },
                    error: function (msg) {
                        console.log(msg);
                    }
                })
            });
        })
    </script>
@endsection