@extends('layouts.app')

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-10">
                        <h4 class="page-header">Список проектов для пользователя {{ $user->name }}</h4>
                    </div>
                    <div class="col-sm-10" style="margin-bottom: 15px;">
                        @permission('create-project')
                        @include('partials.add_project')
                        @endpermission
                    </div>
                    <div class="col-sm-8">
                        <form role="form" id="search-project">
                            <div class="form-group contact-search m-b-30">
                                <input type="text" id="search" name="query" class="form-control" placeholder="Search...">
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div id="project-results">
                        @include('partials.admin.projects_list')
                    </div>
                </div>


                <!-- end row -->

            </div>
            <!-- container -->

        </div>
        <!-- content -->

        <footer class="footer">
            © @php echo date('Y')@endphp. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection



@section('scripts')

    <script>
        $(function () {
            $('#search-project').submit(function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{route('search.project')}}",
                    method: 'POST',
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $('#project-results').html('');
                        $('#project-results').html(data);
                        console.log(data);
                    },
                    error: function (msg) {
                        console.log(msg);
                    }
                })
            });


            $(document).on('click', '.remove-project', function () {
                event.preventDefault();
                var $id = $(this).data('project');
                confirmdelete($id);
            });

            function confirmdelete($id) {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes!'
                }).then(function () {
                    $.ajax({
                        url: "{{route('remove.project')}}",
                        method: 'POST',
                        data: {
                            'id' : $id
                        },
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data != 1) {
                                var message;
                                if (typeof data === 'object') {
                                    for (key in data) {
                                        message += data[key]
                                    }
                                }
                                else {
                                    message = data;
                                }

                                swal({
                                    title: 'Error!',
                                    text: message,
                                    type: 'error',
                                    timer: 3000,
                                    onClose: function () {

                                    }
                                }).catch(swal.noop);
                            }
                            else {
                                swal({
                                    title: 'Success!',
                                    text: 'Проект успешно удален',
                                    type: 'success',
                                    timer: 8000,
                                    onClose: function () {
                                        location.reload();
                                    }
                                }).catch(swal.noop);
                            }
                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    })
                }).catch(swal.noop);
            }


            $('.remove-project').click(function () {
                var $id = $(this).data('project');

            });
            $('#add-pr-form').submit(function (event) {

                event.preventDefault();
                $.ajax({
                    url: "{{route('add.project')}}",
                    method: 'POST',
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        Custombox.close();
                        console.log(data);
                        if (data != 1) {
                            var message;
                            if (typeof data === 'object') {
                                for (key in data) {
                                    message += data[key]
                                }
                            }
                            else {
                                message = data;
                            }

                            swal({
                                title: 'Error!',
                                text: message,
                                type: 'error',
                                timer: 3000,
                                onClose: function () {

                                }
                            }).catch(swal.noop);
                        }
                        else {
                            swal({
                                title: 'Success!',
                                text: 'Проект успешно добавлен',
                                type: 'success',
                                timer: 8000,
                                onClose: function () {
                                        location.reload();
                                }
                            }).catch(swal.noop);
                        }
                    },
                    error: function (msg) {
                        console.log(msg);
                    }
                })
            });
        })
    </script>

@endsection



