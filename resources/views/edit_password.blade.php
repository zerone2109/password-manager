@extends('layouts.app')

@section('content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12">

                        <h4 class="page-title">{{ $title }}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-6">
                                    <form role="form"  id="edit-password" method="POST">
                                        <input type="hidden" name="_Token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="сurrent_password">Current password</label>
                                            <input type="password" class="form-control" name="сurrent_password" id="name" placeholder="Current password" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password">New password</label>
                                            <input type="password" class="form-control" name="password"  placeholder="New password" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm New Password</label>
                                            <input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm New Password" required>
                                        </div>

                                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                                    </form>
                                    <hr>

                                </div>
                                <div class="col-sm-4">

                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; 2016 - 2018. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
@endsection


@section('scripts')

    <script>
        $(function () {
            $('#edit-password').submit(function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{route('edit.password')}}",
                    method: 'POST',
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        console.log(data);

                        if (data != 1) {
                            swal({
                                title: 'Error!',
                                text: typeof data === 'object' ? data['password'] : data,
                                type: 'error',
                                timer: 6000,
                                onClose: function () {

                                }
                            }).catch(swal.noop);
                        }
                        else {
                            swal({
                                title: 'Success!',
                                text: 'Your password is updated.',
                                type: 'success',
                                timer: 3000,
                                onClose: function () {
                                    location.replace("{{route('show.profile')}}");
                                }
                            }).catch(swal.noop);
                        }
                    },
                    error: function (msg) {
                        console.log(msg);
                    }
                })
            });
        })
    </script>

@endsection