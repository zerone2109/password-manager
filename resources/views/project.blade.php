@extends('layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ route('projects') }}">Назад</a>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <h4 class="m-t-0 header-title"><b>Информация по проекту {{$project->name}}</b></h4>
                            <br>
                            <input type="hidden" name="project_id" value="{{$project->id}}">

                            @permission('create-project')
                                @include('partials.add_access')
                            @endpermission

                            @role(['admin', 'super_admin', 'developer'])
                               @isset($project->ftp)
                                   @foreach($project->ftp as $ftp)
                                        <form class="ftp_form">
                                       <div class="form-group">
                                    <h4 class="header-title">
                                        ftp {{$ftp->name}}
                                    </h4>


                                    <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="ftp_host_{{$ftp->id}}" name="ftp_host_{{$ftp->id}}" class="form-control" value="{{$ftp->host}}">
                                                       <input type="hidden" id="access_id_{{$ftp->id}}" name="access_id_{{$ftp->id}}" class="form-control" value="{{$ftp->id}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#ftp_host_{{$ftp->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                     </div>
                                    <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="ftp_login_{{$ftp->id}}" name="ftp_login_{{$ftp->id}}" class="form-control" value="{{$ftp->login}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#ftp_login_{{$ftp->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>
                                    <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="ftp_password_{{$ftp->id}}" name="ftp_password_{{$ftp->id}}" class="form-control" value="{{$ftp->password}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#ftp_password_{{$ftp->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>
                                </div>
                                   @permission('edit-project')
                                       <button class="btn btn-primary waves-effect waves-light">
                                                Сохранить
                                                <i class="md md-save"></i>
                                            </button>


                                            <a  href="javascript:void(0);" data-id="{{$ftp->id}}" data-access="ftp" class="btn btn-danger remove-access waves-effect waves-light">
                                                Удалить
                                                <i class="md md-delete"></i>
                                            </a>
                                   @endpermission
                                   </form>
                                        <hr>
                                   @endforeach
                               @endisset

                               @isset($project->hosting)
                                   @foreach($project->hosting as $hosting)
                                        <form class="hosting_form">
                                      <div class="form-group">
                                               <h4 class="header-title">
                                                   Hosting
                                               </h4>
                                          <div class="form-group row">
                                              <div class="col-md-6">
                                                  <div class="input-group">
                                                      <!-- Target -->
                                                      <input type="hidden" id="access_id_{{$hosting->id}}" name="access_id_{{$hosting->id}}" class="form-control" value="{{$hosting->id}}">
                                                      <input type="text" id="hosting_host_{{$hosting->id}}" name="hosting_host_{{$hosting->id}}" class="form-control" value="{{$hosting->hosting_panel}}">
                                                      <div class="input-group-btn">
                                                          <!-- Trigger -->
                                                          <button class="btn btn-primary waves-effect" data-clipboard-target="#hosting_host_{{$hosting->id}}" >
                                                              <i class="md md-content-copy"></i>
                                                          </button>
                                                      </div>
                                                    </div>
                                              </div>

                                          </div>
                                          <div class="form-group row">
                                              <div class="col-md-6">
                                                  <div class="input-group">
                                                      <!-- Target -->
                                                      <input type="text" id="hosting_login_{{$hosting->id}}" name="hosting_login_{{$hosting->id}}" class="form-control" value="{{$hosting->login}}">
                                                      <div class="input-group-btn">
                                                          <!-- Trigger -->
                                                          <button class="btn btn-primary waves-effect" data-clipboard-target="#hosting_login_{{$hosting->id}}" >
                                                              <i class="md md-content-copy"></i>
                                                          </button>
                                                      </div>
                                                  </div>
                                              </div>

                                          </div>

                                          <div class="form-group row">
                                              <div class="col-md-6">
                                                  <div class="input-group">
                                                      <!-- Target -->
                                                      <input type="text" id="hosting_password_{{$hosting->id}}" name="hosting_password_{{$hosting->id}}" class="form-control" value="{{$hosting->password}}">
                                                      <div class="input-group-btn">
                                                          <!-- Trigger -->
                                                          <button class="btn btn-primary waves-effect" data-clipboard-target="#hosting_password_{{$hosting->id}}">
                                                              <i class="md md-content-copy"></i>
                                                          </button>
                                                      </div>
                                                  </div>
                                              </div>

                                          </div>

                                           </div>
                                            @permission('edit-project')
                                            <button class="btn btn-primary waves-effect waves-light">
                                                Сохранить
                                                <i class="md md-save"></i>
                                            </button>
                                            <a  href="javascript:void(0);" data-id="{{$hosting->id}}" data-access="hosting" class="btn btn-danger remove-access waves-effect waves-light">
                                                Удалить
                                                <i class="md md-delete"></i>
                                            </a>
                                            @endpermission
                                            <hr>
                                        </form>
                                   @endforeach
                                @endisset
                            @endrole
                           @isset($project->admin_site)
                                   @foreach($project->admin_site as $admin_site)
                                    <form class="admin_site">
                                       <div class="form-group">
                                           <h4 class="header-title">
                                               Site admin panel
                                           </h4>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="hidden" id="access_id_{{$admin_site->id}}" name="access_id_{{$admin_site->id}}" class="form-control" value="{{$admin_site->id}}">
                                                       <input type="text" id="site_admin_{{$admin_site->id}}" name="site_admin_{{$admin_site->id}}" class="form-control" value="{{$admin_site->site_adminpanel_url}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#site_admin_{{$admin_site->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="site_login_{{$admin_site->id}}" name="site_login_{{$admin_site->id}}" class="form-control" value="{{$admin_site->login}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#site_login_{{$admin_site->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="site_password_{{$admin_site->id}}" name="site_password_{{$admin_site->id}}" class="form-control" value="{{$admin_site->password}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#site_password_{{$admin_site->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>

                                       </div>
                                        @permission('edit-project')
                                        <button class="btn btn-primary waves-effect waves-light">
                                            Сохранить
                                            <i class="md md-save"></i>
                                        </button>
                                        <a  href="javascript:void(0);" data-id="{{$admin_site->id}}" data-access="site" class="btn btn-danger remove-access waves-effect waves-light">
                                            Удалить
                                            <i class="md md-delete"></i>
                                        </a>
                                        @endpermission
                                        <hr>
                                    </form>
                                   @endforeach
                               @endisset
                           @isset($project->other_access)
                                   @foreach($project->other_access as $other_access)
                                    <form class="other_access">
                                       <div class="form-group">
                                           <h4 class="header-title">
                                               {{$other_access->name}}
                                           </h4>
                                           <p class="text-muted m-b-30 font-13">
                                               {{$other_access->description}}
                                           </p>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="hidden" id="access_id_{{$other_access->id}}" name="access_id_{{$other_access->id}}" class="form-control" value="{{$other_access->id}}">
                                                       <input type="text" id="other_access_url_{{$other_access->id}}" name="other_access_url_{{$other_access->id}}" class="form-control" value="{{$other_access->url}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#other_access_url_{{$other_access->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="other_access_login_{{$other_access->id}}" name="other_access_login_{{$other_access->id}}" class="form-control" value="{{$other_access->login}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#other_access_login_{{$other_access->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="form-group row">
                                               <div class="col-md-6">
                                                   <div class="input-group">
                                                       <!-- Target -->
                                                       <input type="text" id="other_access_password_{{$other_access->id}}" name="other_access_password_{{$other_access->id}}" class="form-control" value="{{$other_access->password}}">
                                                       <div class="input-group-btn">
                                                           <!-- Trigger -->
                                                           <button class="btn btn-primary waves-effect" data-clipboard-target="#other_access_password_{{$other_access->id}}" >
                                                               <i class="md md-content-copy"></i>
                                                           </button>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>

                                       </div>
                                        @permission('edit-project')
                                        <button class="btn btn-primary waves-effect waves-light">
                                            Сохранить
                                            <i class="md md-save"></i>
                                        </button>
                                        <a  href="javascript:void(0);" data-id="{{$other_access->id}}" data-access="other" class="btn btn-danger remove-access waves-effect waves-light">
                                            Удалить
                                            <i class="md md-delete"></i>
                                        </a>
                                        @endpermission
                                        <hr>
                                    </form>
                                   @endforeach
                               @endisset
                           <br>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            © @php echo date('Y')@endphp. All rights reserved.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

@endsection


@section('scripts')

    <script>
        $(function () {
            $(document).on('click', '.remove-access', function () {
                event.preventDefault();
                var $id = $(this).data('id');
                var $type = $(this).data('access');
                confirmdelete($id,$type);
            });

            function confirmdelete($id,$type) {
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes!'
                }).then(function () {
                    $.ajax({
                        url: '{{route('delete.access')}}',
                        method: 'POST',
                        data: {
                            'id' : $id,
                            'access_type' : $type,
                        },
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (msg) {
                            console.log(msg);
                        }
                    });
                    swal({
                        title: 'Deleted!',
                        text: 'access has been deleted.',
                        type: 'success',
                        timer: 3000,
                        onClose: function () {
                            location.reload();
                        }
                    });
                }).catch(swal.noop);
            }
        })

    </script>

@endsection
