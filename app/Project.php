<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $table = 'projects';


    // relations
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('id');;
    }

    public function ftp()
    {
        return $this->hasMany('App\Ftp_accesses');
    }

    public function admin_site()
    {
        return $this->hasMany('App\Site_adminpanel_accesses');
    }

    public function hosting()
    {
        return $this->hasMany('App\Hosting_accesses');
    }

    public function other_access()
    {
        return $this->hasMany('App\Other_accesses');
    }


}
