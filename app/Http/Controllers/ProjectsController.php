<?php

namespace App\Http\Controllers;

use App\Project;
use App\Ftp_accesses;
use App\Other_accesses;
use App\Site_adminpanel_accesses;
use App\Hosting_accesses;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use View;
use DB;
use App\Role;
use App\Permission;

class ProjectsController extends Controller
{
    //


    public function index(){
        $user = Auth::user();
        $data['projects'] = Project::orderBy('id','desc')->paginate(12);

        /* $admin = Role::find(11);
         $S_admin = Role::find(13);
         $manager = Role::find(14);

         $create_pr = Permission::find(1);
         $view_pr = Permission::find(2);
         $edit_pr = Permission::find(3);
         $delete_pr = Permission::find(4);
         $edit_user = Permission::find(5);


         $S_admin->attachPermissions(array($create_pr, $view_pr,$edit_pr,$delete_pr,$edit_user));
         $admin->attachPermissions(array($create_pr, $view_pr,$edit_pr,$delete_pr));
         $manager->attachPermissions(array($view_pr));*/


        $data['user'] = Auth::user();
        return View::make("projects")
            ->with($data)
            ->render();
    }

    public function show($project_id=1){

        $data['project'] = Project::find($project_id);
        $data['user'] = Auth::user();

        return View::make("project")
            ->with($data)
            ->render();
    }

    public function add_project(Request $request){
        $pr = new Project();

        $pr->name = $request->name;
        $pr->description = $request->description ? $request->description : '' ;
        $pr->save();

        return 1;

    }

    public function remove_project(Request $request){


        $pr = Project::find($request->id);
        $pr->delete();

        return 1;

    }

    public function update_project(Request $request){
        $access_type = $request->access_type;
        switch ($access_type){
            case "ftp":
                $ftp = Ftp_accesses::find($request->access_id) ;
                $ftp->host = $request->host;
                $ftp->login = $request->login;
                $ftp->password = $request->password;
                $ftp->save();
                break;
            case "hosting":
                $host = Hosting_accesses::find($request->access_id) ;
                $host->hosting_panel = $request->host;
                $host->login = $request->login;
                $host->password = $request->password;
                $host->save();
                break;
            case "site":
                $site = Site_adminpanel_accesses::find($request->access_id) ;
                $site->site_adminpanel_url = $request->host;
                $site->login = $request->login;
                $site->password = $request->password;
                $site->save();
                break;
            case "other":
                $other = Other_accesses::find($request->access_id) ;
                $other->url = $request->host;
                $other->login = $request->login;
                $other->password = $request->password;
                $other->save();
                break;
        }

       return 'zbs';
    }

    //Добавление данных по проекту
    public function add_access(Request $request){
        $access_type = $request->access_type;

        switch ($access_type) {
            case "ftp":
                $ftp = new Ftp_accesses ;
                $ftp->project_id = $request->project_id;
                $ftp->name = $request->name;
                $ftp->host = $request->host;
                $ftp->login = $request->login;
                $ftp->password = $request->password;
                $ftp->save();
                break;
            case "hosting":
                $host =  new Hosting_accesses ;
                $host->project_id = $request->project_id;
                $host->hosting_panel = $request->host;
                $host->login = $request->login;
                $host->password = $request->password;
                $host->save();
                break;
            case "site":
                $site = new Site_adminpanel_accesses ;
                $site->project_id = $request->project_id;
                $site->site_adminpanel_url = $request->host;
                $site->login = $request->login;
                $site->password = $request->password;
                $site->save();
                break;
            case "other":
                $other = new Other_accesses;
                $other->project_id = $request->project_id;
                $other->name = $request->name;
                $other->description =$request->description;
                $other->url = $request->host;
                $other->login =$request->login;
                $other->password = $request->password;
                $other->save();
                break;

        }
    }

    //Добавление данных по проекту
    public function delete_access(Request $request){
        $access_type = $request->access_type;
        switch ($access_type) {
            case "ftp":
                $ftp = Ftp_accesses::find($request->id);
                $ftp->delete();
                break;
            case "hosting":
                $host =  Hosting_accesses::find($request->id);
                $host->delete();
                break;
            case "site":
                $site =  Site_adminpanel_accesses::find($request->id);
                $site->delete();
                break;
            case "other":
                $other =  Other_accesses::find($request->id);
                $other->delete();
                break;
        }
    }




    //Получение рейтинга проекта к юзеру
    public function get_rating($project, $user){

        //$rating
    }


}
