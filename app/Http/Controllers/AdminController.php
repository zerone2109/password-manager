<?php

namespace App\Http\Controllers;

use App\Post;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use View;
use App\Role;
use App\Permission;
use Auth;
use DB;


class AdminController extends Controller
{

    //middleware
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    /**
     * @return mixed
     */
    public function index(){
        $data['users'] = User::all();
        $data['posts'] = Post::all();

        return View::make("admin.dashboard")
            ->with($data)
            ->render();
    }

   /* public function get_admins(){
        $data['users'] = $this->get_users_by_role('admin');
        $data['roles'] = Role::all();
        $data['role'] = 1;
        $data['title'] = 'Admins';
        return View::make("admin.users")
            ->with($data)
            ->render();
    }

    public function get_editors(){
        $data['title'] = 'Editors';
        $data['roles'] = Role::all();
        $data['role'] = 2;
        $data['users'] = $this->get_users_by_role('editor');
        return View::make("admin.users")
            ->with($data)
            ->render();
    }*/

    public function get_users_by_role($role){
        $users = User::whereHas('roles', function ($query) use($role){
            $query->where('roles.name', '=', $role);
        })->where(function($user) {
            $user->where('id','!=',Auth::user()->id );
        })->paginate(10);
        return $users;
    }

    public function get_users(){
        $data['roles'] = Role::all();
        $data['users'] = User::paginate(10);

        return View::make("admin.users")
            ->with($data)
            ->render();
    }

    public function show_user($id){
        $user = User::find($id);
        if(Auth::user()->id == $id){
            return \redirect(route('show.profile'));
        }
        $data['roles'] = Role::all();
        $data['user'] = $user;
        return View::make("admin.user")
            ->with($data)
            ->render();
    }

    public function search(Request $request){
        $query = strtolower($request->input('query'));
            $data['users'] = User::where('name','like','%'.$query.'%')
                        ->orWhere('email','like','%'.$query.'%')
                    ->paginate(10);

            $data['users'] = $data['users']->count() != 0 ? $data['users'] : 'Nothing found';

        return View::make("partials.admin.users_list")
            ->with($data)
            ->render();
    }

    public function search_project(Request $request){
        $query = strtolower($request->input('query'));
        $data['projects'] = Project::where('name','like','%'.$query.'%')
            ->orWhere('description','like','%'.$query.'%')
            ->paginate(10);

        $data['projects'] = $data['projects']->count() != 0 ? $data['projects'] : 'Nothing found';

        return View::make("partials.admin.projects_list")
            ->with($data)
            ->render();
    }
}
