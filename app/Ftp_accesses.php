<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ftp_accesses extends Model
{
    //

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
