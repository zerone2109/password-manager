<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtpAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('ftp_accesses')) {
            Schema::create('ftp_accesses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id');
                $table->string('host', 64);
                $table->string('	login', 64);
                $table->string('	password', 64);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('ftp_accesses');
    }
}
