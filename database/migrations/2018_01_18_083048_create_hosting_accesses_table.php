<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('hosting_accesses')) {
            Schema::create('hosting_accesses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id');
                $table->string('hosting_panel', 64);
                $table->string('	login', 64);
                $table->string('	password', 64);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hosting_accesses');
    }
}
