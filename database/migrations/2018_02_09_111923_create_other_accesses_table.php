<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('other_accesses')) {
            Schema::create('other_accesses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id');
                $table->string('name', 64);
                $table->string('description', 255);
                $table->string('url', 64);
                $table->string('	login', 64);
                $table->string('	password', 64);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::dropIfExists('other_accesses');
    }
}
