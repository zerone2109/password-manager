<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteAdminpanelAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('site_adminpanel_accesses')) {
            Schema::create('site_adminpanel_accesses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('project_id');
                $table->string('site_adminpanel_url', 64);
                $table->string('	login', 64);
                $table->string('	password', 64);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('site_adminpanel_accesses');
    }
}
