<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('projects'));
});

Route::get('/home', function () {
    return redirect(route('projects'));
});



Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Auth::routes();

Route::group(['middleware' => 'auth'],function(){
    Route::get('/projects','ProjectsController@index')->name('projects');
    //Route::get('/project/{project_id?}','ProjectsController@show')->name('show');
    Route::any('/project/{project_id?}','ProjectsController@show')->name('show');

});

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/users', 'AdminController@get_users')->name('admin.users');
    Route::get('/user/{id}', 'AdminController@show_user')->name('admin.user');
});
Route::get('/show-profile', 'UserController@show')->middleware('auth')->name('show.profile');
Route::get('/change-password', 'UserController@edit_password')->middleware('auth')->name('change.password');
Route::post('/edit-profile', 'UserController@edit')->middleware('auth')->name('edit.profile');
Route::post('/edit-password', 'UserController@change_password')->middleware('auth')->name('edit.password');

Route::post('/add-user', 'UserController@add_user')->middleware('role:admin')->name('add.user');
Route::post('/edit-user/', 'UserController@edit_user')->middleware('role:admin')->name('edit.user');
Route::post('/delete-user/{id}', 'UserController@delete_user')->middleware('role:admin')->name('delete.user');
Route::post('/search-user', 'AdminController@search')->middleware('role:admin')->name('search.user');



Route::post('update-project','ProjectsController@update_project')->name('update.project');
Route::post('add-project','ProjectsController@add_project')->name('add.project');
Route::post('remove-project','ProjectsController@remove_project')->name('remove.project');
Route::post('add-access','ProjectsController@add_access')->name('add.access');
Route::post('delete-access','ProjectsController@delete_access')->name('delete.access');
Route::post('/search-project', 'AdminController@search_project')->middleware('role:admin')->name('search.project');

// Откл регистрацию.
// Route::get('/register', function () {
//     return redirect('/projects');
// });